package com.example.kostya_m.samplelongpolling.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.example.kostya_m.samplelongpolling.R
import com.example.kostya_m.samplelongpolling.api.ChatMessage
import org.jetbrains.anko.find

/**
 * Created by Kostya_M on 09.10.2016.
 */
class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val user: TextView = itemView.find(R.id.message_item_user)
    val msgText: TextView = itemView.find(R.id.message_item_text)

    fun bind(item: ChatMessage) {
        user.text = item.sender
        msgText.text = item.text
    }
}