package com.example.kostya_m.samplelongpolling.api

import java.util.*
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Kostya_M on 01.10.2016.
 */

class ChatMessage (val sender: String, val text: String) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<ChatMessage> = object : Parcelable.Creator<ChatMessage> {
            override fun createFromParcel(source: Parcel): ChatMessage = ChatMessage(source)
            override fun newArray(size: Int): Array<ChatMessage?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(sender)
        dest?.writeString(text)
    }
}