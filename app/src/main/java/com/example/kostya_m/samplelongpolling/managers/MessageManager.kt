package com.example.kostya_m.samplelongpolling.managers

import com.example.kostya_m.samplelongpolling.api.ChatMessage
import com.example.kostya_m.samplelongpolling.api.MessagesRestAPI
import java.util.*

/**
 * Created by Kostya_M on 01.10.2016.
 */
class MessageManager (private val messageService: MessagesRestAPI = MessagesRestAPI()) {

//    fun getMessages(): List<ChatMessage> {
//        var messages = mutableListOf<ChatMessage>()
//        for (i in 1..100) {
//            messages.add(ChatMessage("test", "message" + i))
//        }
//        return messages
//    }

    fun sendMessage(message: ChatMessage) = messageService.sendMessage(message)
    fun getMessages() = messageService.getMessages()
}