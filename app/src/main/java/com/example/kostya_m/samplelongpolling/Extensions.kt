package com.example.kostya_m.samplelongpolling

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Kostya_M on 01.10.2016.
 */

fun ViewGroup.inflate(layoutId: Int): View {
    return LayoutInflater.from(context).inflate(layoutId, this, false);
}
