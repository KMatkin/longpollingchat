package com.example.kostya_m.samplelongpolling.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.example.kostya_m.samplelongpolling.api.ChatMessage
import com.example.kostya_m.samplelongpolling.view.MessageItemUI
import com.example.kostya_m.samplelongpolling.view.MessageViewHolder
import org.jetbrains.anko.AnkoContext
import java.util.*

/**
 * Created by Kostya_M on 01.10.2016.
 */
class MessageAdapter: RecyclerView.Adapter<MessageViewHolder>() {
    private var items: ArrayList<ChatMessage>

    init {
        items = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MessageViewHolder? =
        MessageViewHolder(MessageItemUI().createView(AnkoContext.create(parent!!.context, parent)))


    override fun onBindViewHolder(holder: MessageViewHolder?, position: Int) {
        holder!!.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun addMessages(messages: List<ChatMessage>) {
        items.addAll(messages)
        notifyDataSetChanged()
    }

    fun getMessages(): List<ChatMessage> {
        return items
    }

    fun clearAndAddMessages(messages: List<ChatMessage>) {
        items.clear()
        addMessages(messages)
    }
}