package com.example.kostya_m.samplelongpolling.api

import rx.Observable

/**
 * Created by Kostya_M on 01.10.2016.
 */
interface MessagesAPI {
    fun getMessages(): Observable<List<ChatMessage>>
    fun sendMessage(message: ChatMessage): Unit
}