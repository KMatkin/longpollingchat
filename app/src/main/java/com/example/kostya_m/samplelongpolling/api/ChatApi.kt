package com.example.kostya_m.samplelongpolling.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import rx.Observable

/**
 * Created by Kostya_M on 01.10.2016.
 */
interface ChatApi {
    @POST(".") fun sendMessage(@Body message: ChatMessage): Call<ResponseBody>
    @GET(".") fun getMessages(): Observable<List<ChatMessage>>
}