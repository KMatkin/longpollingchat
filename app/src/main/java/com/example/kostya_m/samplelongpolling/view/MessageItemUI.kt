package com.example.kostya_m.samplelongpolling.view

import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.kostya_m.samplelongpolling.R
import org.jetbrains.anko.*

/**
 * Created by Kostya_M on 09.10.2016.
 */
class MessageItemUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                orientation = LinearLayout.HORIZONTAL
                textView {
                    id = R.id.message_item_user
                    lparams(width = wrapContent) {
                        marginEnd = dip(10)
                    }
                }
                textView {
                    id = R.id.message_item_text
                    lparams(width = 0, weight = 1.0f)
                    typeface = Typeface.DEFAULT_BOLD
                }
            }
        }
    }
}