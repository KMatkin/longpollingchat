package com.example.kostya_m.samplelongpolling.api

import android.util.Log
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import rx.Observable

/**
 * Created by Kostya_M on 01.10.2016.
 */
class MessagesRestAPI: MessagesAPI {
    companion object {
        private val TAG = "MessagesRestAPI"
        private val API_URL = "http://chat.kilyakov.xyz:5051/api/chat/"
    }
    private val chatApi: ChatApi

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor(logging)
        val retrofit = Retrofit.Builder()
                        .baseUrl(API_URL)
                        .addConverterFactory(MoshiConverterFactory.create())
                        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                        .client(httpClient.build())
                        .build()
        chatApi = retrofit.create(ChatApi::class.java)
    }

    override fun getMessages(): Observable<List<ChatMessage>> {
        return chatApi.getMessages().retry().repeat()
    }

    override fun sendMessage(message: ChatMessage) {
        chatApi.sendMessage(message).enqueue(object: Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>?, response: retrofit2.Response<ResponseBody>?) {
                Log.d(TAG, "Send message $message. Result=${response?.isSuccessful}")
            }
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                t?.printStackTrace()
            }
        })
    }
}