package com.example.kostya_m.samplelongpolling

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import com.example.kostya_m.samplelongpolling.adapters.MessageAdapter
import com.example.kostya_m.samplelongpolling.api.ChatMessage
import com.example.kostya_m.samplelongpolling.managers.MessageManager
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.util.*

class MainActivity : AppCompatActivity() {
    companion object {
        private val TAG = "MainActivity"
        private val KEY_MESSAGES = "key_messages"
    }

    private var messageLog : RecyclerView? = null

    private var messageEdit : EditText? = null

    private val messageAdapter = MessageAdapter()

    private val messageManager = MessageManager()

    private var subscriptions = CompositeSubscription()

    private var messages: List<ChatMessage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainActivityUI().setContentView(this)

        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_MESSAGES)) {
            messages = savedInstanceState.getParcelableArrayList(KEY_MESSAGES)
            messageAdapter.clearAndAddMessages(messages!!)
        }
    }

    override fun onResume() {
        super.onResume()
        subscriptions = CompositeSubscription()
        val chatMessagesSubscription = messageManager.getMessages()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { messages ->
                            messageAdapter.addMessages(messages)
                            messageLog?.scrollToPosition(messageAdapter.itemCount-1)
                        },
                        { e ->
                            Log.e(TAG, "${e.message}")
                        }
                    )
        subscriptions.add(chatMessagesSubscription)
    }

    override fun onPause() {
        super.onPause()
        subscriptions.clear()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val messages = messageAdapter.getMessages()
        val m: ArrayList<ChatMessage> = ArrayList(messages)
        outState.putParcelableArrayList(KEY_MESSAGES, m)
    }





    inner class MainActivityUI : AnkoComponent<MainActivity> {
        override fun createView(ui: AnkoContext<MainActivity>): View {
            val view = with (ui) {
                verticalLayout {
                    orientation = LinearLayout.VERTICAL
                    messageLog = recyclerView {
                        layoutManager = LinearLayoutManager(applicationContext)
                        adapter = messageAdapter
                    }.lparams(height = 0, weight = 1.0f)
                    messageEdit = editText {
                        hint = "Message"
                        inputType = EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT
                        imeOptions = EditorInfo.IME_ACTION_SEND
                        onEditorAction { textView, actionId, keyEvent ->
                            if (actionId == EditorInfo.IME_ACTION_SEND && text.toString() != "") {
                                messageManager.sendMessage(ChatMessage("android", text.toString()))
                                messageLog?.scrollToPosition(messageAdapter.itemCount - 1)
                                hideKeyboard()
                                true
                            } else false
                        }
                    }
                }
            }
            setupUI(view)
            return view
        }
        fun setupUI(view: View) {

            // Set up touch listener for non-text box views to hide keyboard.
            if (view !is EditText) {
                view.setOnTouchListener { v, event ->
                    hideKeyboard()
                    false
                }
            }

            //If a layout container, iterate over children and seed recursion.
            if (view is ViewGroup) {
                for (i in 0..view.childCount - 1) {
                    val innerView = view.getChildAt(i)
                    setupUI(innerView)
                }
            }
        }
        fun hideKeyboard() {
            val view = currentFocus
            if (view != null) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
            messageEdit?.setText("")
        }
    }
}
